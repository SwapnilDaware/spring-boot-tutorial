package com.demo.spring.demospring.controller;
// import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;

// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.demo.spring.demospring.exception.AlreadyExistException;
import com.demo.spring.demospring.exception.ErrorResponse;
import com.demo.spring.demospring.exception.RecordNotFound;
import com.demo.spring.demospring.exception.StorageException;
import com.demo.spring.demospring.exception.StorageFileNotFoundException;
import com.demo.spring.demospring.model.Employee;
import com.demo.spring.demospring.model.Response;
import com.demo.spring.demospring.repository.EmployeeRepo;
import com.demo.spring.demospring.service.FileStorageService;

@RestController
@RequestMapping
public class EmployeeController {

    @Autowired
    EmployeeRepo employeRepo;

    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping
    ResponseEntity<?> addEmployee(@RequestBody Employee employee) throws AlreadyExistException{
        if(employeRepo.findByName(employee.getName())!=null){
            throw new AlreadyExistException("Employee with name "+employee.getName()+" is already present");
        }
        employeRepo.save(employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    //already exist
    @ExceptionHandler(AlreadyExistException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ResponseEntity<?> handleAlreadfyExistException(AlreadyExistException exception){
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.CONFLICT, "Already exist exception", exception.getMessage());

        return new ResponseEntity<>(errorResponse,HttpStatus.CONFLICT);

    }

    //record not found handler method
    @ExceptionHandler(RecordNotFound.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    ResponseEntity<?> handleRecordNotFoundException(RecordNotFound exception){
        ErrorResponse errorResponse = new ErrorResponse(HttpStatus.CONFLICT, "Recortd not found", exception.getMessage());

        return new ResponseEntity<>(errorResponse,HttpStatus.CONFLICT);

    }


    @GetMapping
    List<Employee> getEmployee() {
        List<Employee> employee = employeRepo.findAll();
        return employee;
    }

    // http://localhost:8080/update/7
    @PutMapping("/update/{id}")
    ResponseEntity<Employee> updatEmployee(@PathVariable Integer id, @RequestBody Employee updatedEmployee) {

        if(employeRepo.findById(id).isPresent()){

        Employee employee = employeRepo.findById(id).get();

        employee.setName(updatedEmployee.getName());

        Employee updatedEmp = employeRepo.save(employee);

        return ResponseEntity.ok(updatedEmp);

        }else{
            throw new RecordNotFound("Employee with id "+id+" not found");
        }
    }

    // delete mappin
    @DeleteMapping("/delete/{id}")
    ResponseEntity<String> deleteEmployee(@PathVariable Integer id) throws Exception {
        Employee employee = employeRepo.findById(id).get();
        if (employee == null) {
            throw new Exception("Employee not found");
        }

        employeRepo.deleteById(id);
        return ResponseEntity.ok("Employee record deleted successfully!");

    }

    // patch mapping
    @PatchMapping("/patch/{id}")
    ResponseEntity<?> patchEmployee(@PathVariable Integer id, @RequestBody Employee updatedEmployee) {

        Employee employee = employeRepo.findById(id).get();
        // throw new ExceptionClass("Employee not found");

        employee.setName(updatedEmployee.getName());

        Employee updatedEmp = employeRepo.save(employee);

        return ResponseEntity.ok(updatedEmp);

    }

    //file upload rest api
    @PostMapping("/uploadFile")
    public Response uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/downloadFile/")
            .path(fileName)
            .toUriString();

        return new Response(fileName, fileDownloadUri,
            file.getContentType(), file.getSize());
    }

    @PostMapping("/uploadMultipleFiles")
    public List < Response > uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
            .stream()
            .map(file -> uploadFile(file))
            .collect(Collectors.toList());
    }

    
	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
		return ResponseEntity.notFound().build();
	}
    
    
	@ExceptionHandler(StorageException.class)
	public ResponseEntity<?> handleStorageException(StorageException exc) {
		return ResponseEntity.badRequest().build();
	}

}
