package com.demo.spring.demospring.exception;

import org.springframework.http.HttpStatus;

public class ErrorResponse {
    private HttpStatus statusCode;
    private String message;
    private String description;

    public ErrorResponse(HttpStatus conflict,String message, String description){
        super();
        this.statusCode = conflict;
        this.message = message;
        this.description = description;
    }

    
    public HttpStatus getHttpStatus(){
        return statusCode;
    }
    
    public void setCode(HttpStatus errorCode){
        this.statusCode = errorCode;
    }

    public String getMsg(){ //msg
        return message;
    }
    
    public void setMsg(String msg){
        this.message = msg;
    }

    
    public String getDetails(){ //details
        return description;
    }
    
    public void setDesc(String desc){
        this.description = desc;
    }

}
