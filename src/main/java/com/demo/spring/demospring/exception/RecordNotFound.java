package com.demo.spring.demospring.exception;

public class RecordNotFound extends RuntimeException{

    String message;

    public RecordNotFound(){

    }

    public RecordNotFound(String message){
        super(message);
        this.message = message;
    }
    
    public String getMessage(){
        return message;
    }

    
}

